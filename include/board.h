/*
 * board.h
 *
 *  Created on: Aug 12, 2022
 *      Author: rroland
 */

#ifndef INCLUDE_BOARD_H_
#define INCLUDE_BOARD_H_

void board_init(void);

#endif /* INCLUDE_BOARD_H_ */
