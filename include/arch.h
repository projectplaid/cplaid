/*
 * arch.h
 *
 *  Created on: Aug 12, 2022
 *      Author: rroland
 */

#ifndef INCLUDE_ARCH_H_
#define INCLUDE_ARCH_H_

void arch_init(void);

#endif /* INCLUDE_ARCH_H_ */
