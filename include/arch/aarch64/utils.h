/*
 * utils.h
 *
 *  Created on: Aug 13, 2022
 *      Author: rroland
 */

#ifndef INCLUDE_ARCH_AARCH64_UTILS_H_
#define INCLUDE_ARCH_AARCH64_UTILS_H_

extern void delay ( unsigned long);
extern void put32 ( unsigned long, unsigned int );
extern unsigned int get32 ( unsigned long );
extern unsigned long get_el ( void );
extern void set_pgd(unsigned long pgd);
extern unsigned long get_pgd();

#endif /* INCLUDE_ARCH_AARCH64_UTILS_H_ */
