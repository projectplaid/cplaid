/*
 * gpio.h
 *
 *  Created on: Aug 12, 2022
 *      Author: rroland
 */

#ifndef INCLUDE_BSP_RPI3_GPIO_H_
#define INCLUDE_BSP_RPI3_GPIO_H_

#include "bsp/rpi3/peripherals.h"

#define GPFSEL1         (PBASE+0x00200004)
#define GPSET0          (PBASE+0x0020001C)
#define GPCLR0          (PBASE+0x00200028)
#define GPPUD           (PBASE+0x00200094)
#define GPPUDCLK0       (PBASE+0x00200098)

#endif /* INCLUDE_BSP_RPI3_GPIO_H_ */
