/*
 * uart.h
 *
 *  Created on: Aug 12, 2022
 *      Author: rroland
 */

#ifndef INCLUDE_HAL_UART_H_
#define INCLUDE_HAL_UART_H_

#include <stdint.h>

void uart_init(void);
char uart_recv(void);
void uart_send(char c);

#endif /* INCLUDE_HAL_UART_H_ */
