/*
 * board.c
 *
 *  Created on: Aug 12, 2022
 *      Author: rroland
 */

#include <stddef.h>

#include "board.h"

#include "bsp/rpi3/peripherals.h"
#include "hal/uart.h"

void board_init(void) {
	uart_init();
}
