CC = aarch64-none-elf-gcc
CFLAGS = -Iinclude -O0 -g -ffreestanding -Wall -Wextra -Werror
LDFLAGS = -nostartfiles -nostdlib
LIBS = -lgcc

ARCH=aarch64
BSP=rpi3

ARCHDIR=src/arch/$(ARCH)
BSPDIR=src/bsp/$(BSP)

CSRCS = $(wildcard src/*.c) \
$(wildcard $(ARCHDIR)/*.c) $(wildcard $(ARCHDIR)/*/*.c) \
$(wildcard $(BSPDIR)/*.c) $(wildcard $(BSPDIR)/*/*.c) \

SSRCS = $(wildcard src/*.S) $(wildcard src/*/*.S) \
$(wildcard $(ARCHDIR)/*.S) $(wildcard $(ARCHDIR)/*/*.S) \
$(wildcard $(BSPDIR)/*.S) $(wildcard $(BSPDIR)/*/*.S) \

COBJS = $(patsubst %.c, %.o, $(CSRCS)) 
SOBJS = $(patsubst %.S, %.o, $(SSRCS))

LINK_LIST=\
$(LDFLAGS) \
$(COBJS) \
$(SOBJS) \
$(LIBS) \

.PHONY: all clean
.SUFFIXES: .o .c .S

all: plaid.elf

plaid.elf: $(COBJS) $(SOBJS) $(ARCHDIR)/linker.ld
	$(CC) -T $(ARCHDIR)/linker.ld -o $@ $(CFLAGS) $(LINK_LIST)

%.o: %.c
	$(CC) -c $< -o $@ -std=gnu18 $(CFLAGS) $(CPPFLAGS)

%.o: %.S
	$(CC) -c $< -o $@ $(CFLAGS) $(CPPFLAGS)

clean:
	rm -f plaid.elf
	rm -f $(SOBJS) $(COBJS) #*.o */*.o */*/*.o
#	rm -f  *.d */*.d */*/*.d
